<?php get_header(); ?>

    <section class="section-block">
        <div class="container">

            <?php if ( have_posts() ) : ?>

                <?php
                $query = new WP_Query( 'cat=2' );
                if( $query -> have_posts()) :?>
                    <h2 class="category-ttl"><?php echo get_cat_name( 2); ?></h2>
                    <div class="blog-block-wrap">
                    <?php while ( $query->have_posts() ) : $query -> the_post(); ?>

                    <div class="blog-block <?php if( get_field('hightlight') ): ?>active<?php endif; ?>">
                        <div class="blog-block-img">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="logo">
                        </div>
                        <div class="blog-block-descr">
                            <div class="blog-block-ttl">
                                <div class="blog-block-ttl-left">
                                    <h3><?php the_title()?></h3>

                                    <?php if( get_field('post_link') ): ?>
                                        <a href="<?php the_field('post_link'); ?>">(<?php the_field('post_link'); ?>)</a>
                                    <?php endif; ?>
                                </div>
                                <div class="blog-block-ttl-right">
                                    <!--<div class="stars-wrap"></div>-->
                                    <?php /*if(function_exists('the_ratings')) { the_ratings(); } */?>
                                    <!--<div class="stars-numb">()</div>-->

                                    <?php if( get_field('hightlight') ): ?>
                                        <span class="blog-vendor">FEATURED VENDOR</span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <?php the_excerpt() ?>

                            <div class="blog-block-bottom">
                                <div class="blog-block-bottom-i">
                                    <div class="btn-wrap">
                                        <?php
                                        if(get_the_tags()) {
                                            foreach (get_the_tags() as $tag) {

                                                ?>
                                                <a href="<?php echo get_tag_link($tag->term_id); ?>" class="btn btn-default btn-sm"><?php echo $tag->name?></a>
                                            <?php } } ?>
                                    </div>
                                    <?php if( have_rows('notable_customers') ):?>
                                        <div class="bottom-tags"><strong>Notable Customers: </strong>
                                            <?php
                                            while ( have_rows('notable_customers') ) : the_row();
                                                $array[] = get_sub_field('customer');
                                            endwhile;
                                            $foo = implode(', ', $array);
                                            echo $foo;
                                            $array = array();;
                                            $foo = '';
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="blog-block-bottom-i">
                                    <a href="<?php echo  get_permalink()?>" class="btn btn-lg btn-primary">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
                    </div>
                <?php endif;

                $query = new WP_Query( 'cat=3' );
                if( $query -> have_posts()) :?>
                    <h2 class="category-ttl"><?php echo get_cat_name( 3); ?></h2>
                    <div class="blog-block-wrap">
                        <?php while ( $query->have_posts() ) : $query -> the_post(); ?>

                            <div class="blog-block <?php if( get_field('hightlight') ): ?>active<?php endif; ?>">
                                <div class="blog-block-img">
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="logo">
                                </div>
                                <div class="blog-block-descr">
                                    <div class="blog-block-ttl">
                                        <div class="blog-block-ttl-left">
                                            <h3><?php the_title()?></h3>

                                            <?php if( get_field('post_link') ): ?>
                                                <a href="<?php the_field('post_link'); ?>">(<?php the_field('post_link'); ?>)</a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="blog-block-ttl-right">
                                            <!--<div class="stars-wrap"></div>-->
                                            <?php /*if(function_exists('the_ratings')) { the_ratings(); } */?>
                                            <!--<div class="stars-numb">()</div>-->

                                            <?php if( get_field('hightlight') ): ?>
                                                <span class="blog-vendor">FEATURED VENDOR</span>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                    <?php the_excerpt() ?>

                                    <div class="blog-block-bottom">
                                        <div class="blog-block-bottom-i">
                                            <div class="btn-wrap">
                                                <?php
                                                if(get_the_tags()) {
                                                    foreach (get_the_tags() as $tag) {

                                                        ?>
                                                        <a href="<?php echo get_tag_link($tag->term_id); ?>" class="btn btn-default btn-sm"><?php echo $tag->name?></a>
                                                    <?php } } ?>
                                            </div>
                                            <?php if( have_rows('notable_customers') ):?>
                                                <div class="bottom-tags"><strong>Notable Customers: </strong>
                                                    <?php
                                                    while ( have_rows('notable_customers') ) : the_row();
                                                        $array[] = get_sub_field('customer');
                                                    endwhile;
                                                    $foo = implode(', ', $array);
                                                    echo $foo;
                                                    ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="blog-block-bottom-i">
                                            <a href="<?php echo  get_permalink()?>" class="btn btn-lg btn-primary">Learn More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif;

                // Previous/next page navigation.
                the_posts_pagination( array(
                    'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
                    'next_text'          => __( 'Next page', 'twentyfifteen' ),
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
                ) );

            // If no content, include the "No posts found" template.
            else :


            endif;
            ?>


        </div>
    </section>


<?php get_footer(); ?>