<?php
/**
 * Created by IntelliJ IDEA.
 * User: Astargh
 * Date: 19.10.2017
 * Time: 17:28
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,600" rel="stylesheet">

    <title><?php wp_title(); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>" />

    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:300,400,600|Ubuntu:300,400,500" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="layout">

    <!--<header class="header">
        <div class="container">
            <a href="<?/*= esc_url(home_url('/')); */?>" title="<?php /*echo get_bloginfo('name'); */?>" class="logo">
                <?php /*if( get_field('header_logo', option) ): */?>

                    <img src="<?php /*the_field('header_logo', option); */?>"/>

                <?php /*endif; */?>
            </a>
        </div>
    </header>-->

    <?php if ( is_front_page() ) :?>
        <header class="header-alt" style="background-image: url(<?php echo get_template_directory_uri();?>/images/title-bg.jpg);">
            <div class="container">
                <div class="logo">
                    <a href="<?php echo esc_url(home_url('/'));?>" title="<?php /*echo get_bloginfo('name'); */?>">
                        <?php if( get_field('header_logo', option) ): ?>
                            <img src="<?php the_field('header_logo', option); ?>"/>
                        <?php endif; ?>
                    </a>
                </div>
                <div class="row">
                    <div class="col-sm-9">
                        <h1>
                            <?php if( get_field('top_section_ttl', option) ): ?>

                                <?php the_field('top_section_ttl', option); ?>

                            <?php endif; ?>
                        </h1>
                        <p>
                            <?php if( get_field('top_section_text', option) ): ?>

                                <?php the_field('top_section_text', option); ?>

                            <?php endif; ?>
                        </p>
                    </div>
                </div>
            </div>
        </header>
    <?php else :?>
        <div class="header-alt header-secondary" style="background-image: url(<?php echo get_template_directory_uri();?>/images/header-bg.jpg);">
            <div class="container">
                <div class="logo">
                    <a href="<?php echo esc_url(home_url('/'));?>" title="<?php /*echo get_bloginfo('name'); */?>">
                        <?php if( get_field('header_logo', option) ): ?>
                            <img src="<?php the_field('header_logo', option); ?>"/>
                        <?php endif; ?>
                    </a>
                </div>
                <a href="<?php echo esc_url(home_url('/'));?>" class="btn btn-warning-alt">Back to Vendors</a>
            </div>
        </div>
    <?php endif;?>