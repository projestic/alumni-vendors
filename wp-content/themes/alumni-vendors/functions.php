<?php
/**
 * Created by IntelliJ IDEA.
 * User: Astargh
 * Date: 19.10.2017
 * Time: 17:36
 */
# Enqueue Custom Scripts
define('DIR', get_template_directory_uri());
define('PATH_IMG', DIR.'/images/');

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'alumni_wp_enqueue_scripts' );
function alumni_wp_enqueue_scripts() {


    # enqueue custom styles
    wp_enqueue_style( 'app.css', DIR . '/css/app.css', false, filemtime(get_stylesheet_directory().'/css/app.css') );

    # enqueue custom scripts
    wp_enqueue_script('app.js', DIR . '/js/app.js', false,  filemtime(get_stylesheet_directory().'/js/app.js') );
}


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();
    acf_add_options_sub_page('Header');
    acf_add_options_sub_page('Footer');
}

add_theme_support('post-thumbnails');

function custom_rating_image_extension() {
    return 'png';
}
add_filter( 'wp_postratings_image_extension', 'custom_rating_image_extension' );

add_filter( 'tiny_mce_before_init', 'wpex_mce_google_fonts_array' );
function wpex_mce_google_fonts_array( $initArray ) {
    $theme_advanced_fonts = 'Saira Extra Condensed=Saira Extra Condensed;';
    $theme_advanced_fonts .= 'Ubuntu=Ubuntu;';
    $theme_advanced_fonts .= 'Titillium Web=Titillium Web;';
    $theme_advanced_fonts .= 'Roboto=Roboto';
    $initArray['font_formats'] = $theme_advanced_fonts;
    return $initArray;
}

function html($tmpl, $val) {
    if (!empty($val)) {
        echo sprintf($tmpl, $val);
    }
}