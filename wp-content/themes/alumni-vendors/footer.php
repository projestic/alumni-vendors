
<!-- Footer -->

<footer class="footer-alt">
    <div class="container">
        <?php if ( is_front_page() ) :?>
        <div class="info-footer">
            <div class="row custom-row flex-row">
                <div class="col-sm-6 item-holder">
                    <?php html('<div class="item">%s</div>', get_field('footer_left', 'option')); ?>
                </div>
                <div class="col-sm-6 item-holder">
                    <?php html('<div class="item">%s</div>', get_field('footer_right', 'option')); ?>
                </div>
            </div>
        </div>
        <?php else: ?>
            <div class="footer-top"></div>
            <div class="post-footer text-center">
            <a href="#" class="logo">
                <?php if( get_field('footer_logo', 'option') ): ?>

                    <img src="<?php the_field('footer_logo', 'option'); ?>" />

                <?php endif; ?>
                <?php html('<img src="$s" />', get_field('footer_logo', 'option')); ?>
            </a>
            <p>© 2017 AlumniVendors. All Rights Reserved.</p>
        </div>
        <?php endif; ?>
        
    </div>
</footer>
<div class="footer-new">
    <div class="about-box">
        <div class="container">
            <div class="row">
                <?php if (have_rows('footer_button', 'option')): 
                    while (have_rows('footer_button', 'option')): the_row() ;
                
                        ?>
                    <div class="col-sm-6">
                        <div class="item">
                            <p>
                                <?php the_sub_field('footer_button_descr', 'option'); ?>
                            </p>
                            <a href="<?php echo get_sub_field('footer_button_link', 'option'); ?>" class="btn btn-dark" style="background-image: url(<?php echo get_sub_field('footer_button_image', 'option'); ?>)"><?php echo get_sub_field('footer_button_text', 'option'); ?></a>
                        </div>
                    </div>
                <?php endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
    <div class="bottom-block">
        <div class="container">
            <div class="row flex-row">
                <div class="col-sm-4">
                    <div class="text">
                        <p>
                            <?php the_field('copyright_text', 'option'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <?php if( get_field('footer_logo', 'option') ): ?>

                        <a href="<?php echo esc_url(home_url('/'));?>"><img src="<?php the_field('footer_logo', 'option'); ?>" /></a>

                    <?php endif; ?>
                </div>
                <div class="col-sm-4">
                    <?php html('<strong class="subtitle">%s</strong>', get_field('hero_subtitle')); ?>
                    <div class="item">
                        <?php the_field('right_box', 'option'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end Footer -->

</div>
<?php wp_footer(); ?>

</body>
</html>