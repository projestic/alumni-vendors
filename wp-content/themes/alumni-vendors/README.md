## Alumni Vendors ##

#### 1. Setup Instructions:

   a. Clone
   ```
   git clone Astargh@bitbucket.org:projestic/ej.git
   ```

   c. Install dependencies
   ```
   npm install
   bower install
   ```

   d. Build project
   ```
   gulp