<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

        <?php
        // Start the loop.
        while ( have_posts() ) : the_post(); ?>
            <div class="content-holder">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-lg-2">
                        <div class="blog-block-img">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="logo">
                        </div>
                    </div>
                    <div class="col-sm-9 col-lg-10">
                        <div class="blog-block-descr">
                            <div class="blog-block-ttl">
                                <div class="blog-block-ttl-left">
                                    <h3><?php the_title(); ?></h3>
                                    <?php if( get_field('post_link') ): ?>

                                        <a href="<?php the_field('post_link'); ?>">(<?php the_field('post_link'); ?>)</a>

                                    <?php endif; ?>
                                </div>
                                <div class="blog-block-ttl-right">
                                    <?php if( get_field('hightlight') ): ?>
                                        <strong class="title-vendor">FEATURED VENDOR</strong>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="blog-block-bottom">
                                <div class="blog-block-bottom-i">
                                    <div class="btn-wrap">
                                        <?php
                                        if(get_the_tags()) {
                                        foreach (get_the_tags() as $tag) {

                                         ?>
                                            <a href="<?php echo get_tag_link($tag->term_id); ?>" class="btn btn-default btn-sm"><?php echo $tag->name?></a>
                                        <?php } } ?>
                                    </div>
                                    <?php if( have_rows('notable_customers') ):?>
                                        <div class="bottom-tags"><strong>Notable Customers: </strong>
                                            <?php
                                            while ( have_rows('notable_customers') ) : the_row();
                                                $array[] = get_sub_field('customer');
                                            endwhile;
                                            $foo = implode(', ', $array);
                                            echo $foo;
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <?php if( get_field('twitter') || get_field('linkedin') || get_field('facebook') ): ?>
                                <div class="block-socials">
                                    <span class="title-list"><b>Contact:</b></span>
                                    <ul class="list-socials">

                                        <?php if( get_field('twitter') ): ?>
                                            <li>
                                                <a href="<?php the_field('twitter'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                            </li>
                                        <?php endif; ?>

                                        <?php if( get_field('linkedin') ): ?>
                                            <li>
                                                <a href="<?php the_field('linkedin'); ?>"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                                            </li>
                                        <?php endif; ?>

                                        <?php if( get_field('facebook') ): ?>
                                            <li>
                                                <a href="<?php the_field('facebook'); ?>"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                            </li>
                                        <?php endif; ?>

                                    </ul>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="content">
                            <?php the_content(); ?>
                            <div class="content-btn-wrap">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#contact-modal">Request Contact</a>
                            </div>
                        </div>
                        <div class="back-link-wrap">
                            <a href="<?php echo esc_url(home_url('/'));?>" class="btn btn-primary-transp">Back to Vendors</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php endwhile;
        ?>

        </main><!-- .site-main -->
    </div><!-- .content-area -->
    <div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel">Have EnterpriseAlumni Contact You...</h4>
            </div>

            <?php echo do_shortcode('[contact-form-7 id="85" title="Contact form 1"]')?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
